# Ka-Ching.Auto-Builds

Automated builds of the [Fritzing](https://fritzing.org/) desktop application.

## Status

[![pipeline status](https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/badges/master/pipeline.svg)](https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/commits/master)

## Download

End users may please use the latest **release** versions:

-   [CentOS-8-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=CentOS-8-release
    )
-   [Debian-Buster-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Debian-Buster-release
    )
-   [Debian-Bullseye-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Debian-Bullseye-release
    )
-   [Fedora-33-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Fedora-33-release
    )
-   [Fedora-34-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Fedora-34-release
    )
-   [Ubuntu-18.04-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Ubuntu-18.04-release
    )
-   [Ubuntu-20.04-release](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Ubuntu-20.04-release
    )

Developers may please use the latest **master** versions:

-   [CentOS-8-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=CentOS-8-master
    )
-   [Debian-Buster-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Debian-Buster-master
    )
-   [Debian-Bullseye-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Debian-Bullseye-master
    )
-   [Fedora-33-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Fedora-33-master
    )
-   [Fedora-34-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Fedora-34-master
    )
-   [Ubuntu-18.04-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Ubuntu-18.04-master
    )
-   [Ubuntu-20.04-master](
        https://gitlab.com/Ka-Ching/Ka-Ching.Auto-Builds/-/jobs/artifacts/master/browse/fritzing-app?job=Ubuntu-20.04-master
    )

## Donating

If you like these packages, please
    [consider donating](https://fritzing.org/download/).

## Contributing

If you want to help, please
[create a merge request](
    https://gitlab.com/Ka-Ching/ka-ching.auto-builds/-/merge_requests
).

## Schedule

The builds get triggered by [Ka-Ching.Base-Images](
    https://gitlab.com/Ka-Ching/ka-ching.base-images
).

## Licenses

-   The contents of this repository are licensed as GPLv3.
-   The packages build here contain a compiled application code, and a 
    library of electronic parts. For details on their licenses, see [
    their FAQ](https://fritzing.org/faq/), but in short:
    -   The application code is licensed under [GPLv2](
            https://gitlab.com/Ka-Ching/ka-ching.auto-builds/-/blob/master/LICENSE_GPL-2.0.markdown
        ) and [GPLv3](
            https://gitlab.com/Ka-Ching/ka-ching.auto-builds/-/blob/master/LICENSE_GPL-3.0.markdown
        ).
    -   The parts library and the documentation is licensed as [CC-BY-SA](
            https://gitlab.com/Ka-Ching/ka-ching.auto-builds/-/blob/master/LICENSE_CC-BY-SA.markdown
        ).

## Authors

-   The content of this repository was created by [its contributors](https://gitlab.com/Ka-Ching/ka-ching.auto-builds/-/graphs/master).
-   The application packaged here was created by [its contributors](https://github.com/fritzing/fritzing-app/graphs/contributors).
-   The electronic parts library was created by [its contributors](https://github.com/fritzing/fritzing-parts/graphs/contributors).

## Naming

I kind of like [the sound of it](
    https://www.youtube.com/watch?v=iEe3hBXZEyI
).

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
*Ka-Ching!*